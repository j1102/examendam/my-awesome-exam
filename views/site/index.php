<?php

/** @var yii\web\View $this */
use yii\grid\GridView;

$this->title = 'El examen';
?>
<div class="site-index card shadow-sm">
    <div class="card-header">
        <p class="text-center h2 text-muted">The Awesome Exam Of Jorge</p>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <div class="card shadow">
                <div class="card-header">
                    <p class="text-center h4 text-muted">Consulta 1</p>
                </div>
                <div class="card-body">
                    <?= GridView::widget([
                        'dataProvider' => $ciclistas,
                        'columns' => [
                            'nombre',
                            'dorsal',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card shadow">
                <div class="card-header">
                    <p class="text-center h4 text-muted">Consulta 2</p>
                </div>
                <div class="card-body">
                    <?= GridView::widget([
                        'dataProvider' => $etapas,
                        'columns' => [
                            'numetapa',
                            'kms',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card shadow">
                <div class="card-header">
                    <p class="text-center h4 text-muted">Consulta 3</p>
                </div>
                <div class="card-body">
                    <?= GridView::widget([
                        'dataProvider' => $puertos,
                        'columns' => [
                            'nompuerto',
                            'dorsal',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
